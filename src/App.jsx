import { useState } from "react";
import "./App.css";
import { useEffect } from "react";
import axios from "axios";

function App() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get("http://localhost:3000/cars")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        if (typeof error.response === "undefined") {
          setError(
            "Une erreur est survenue. Ton serveur backend est-il bien lancé sur le port 3000 ? Si c'est le cas, tu rencontres peut-être une erreur liée au CORS : ouvre la console de développement de ton navigateur pour le vérifier !"
          );
        } else {
          setError(error.message);
        }
      });
  }, []);

  return (
    <>
      {error === null && data === null && "Chargement..."}
      {error !== null && error}
      {error === null && data !== null && (
        <ul>
          {data.map((eachCar) => (
            <li key={eachCar.car_id}>
              {eachCar.brand} - {eachCar.model}
            </li>
          ))}
        </ul>
      )}
    </>
  );
}

export default App;
